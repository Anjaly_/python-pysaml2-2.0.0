Source: python-pysaml2
Section: python
Priority: optional
Maintainer: PKG OpenStack <openstack-devel@lists.alioth.debian.org>
Uploaders: Thomas Goirand <zigo@debian.org>
Build-Depends: debhelper (>= 9),
               python-all (>= 2.6.6-3~),
               python-setuptools,
               python-sphinx
Build-Depends-Indep: python-crypto,
                     python-dateutil,
                     python-defusedxml,
                     python-decorator,
                     python-mako,
                     python-memcache,
                     python-pymongo,
                     python-openssl,
                     python-paste,
                     python-pyasn1,
                     python-pytest,
                     python-repoze.who,
                     python-requests,
                     python-tz,
                     python-zope.interface,
                     xmlsec1
Standards-Version: 3.9.5
X-Python-Version: >= 2.7
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=openstack/python-pysaml2.git
Vcs-Git: git://anonscm.debian.org/openstack/python-pysaml2.git
Homepage: https://github.com/rohe/pysaml2

Package: python-pysaml2
Architecture: all
Pre-Depends: dpkg (>= 1.15.6~)
Depends: python-defusedxml,
         python-mako,
         python-memcache,
         python-pymongo,
         python-pyasn1,
         python-repoze.who,
         xmlsec1,
         ${misc:Depends},
         ${python:Depends}
Description: SAML Version 2 to be used in a WSGI environment - Python 2.x
 This package provides a Python implementation of SAML Version 2 to be used in
 a WSGI environment.
 .
 From wikipedia: Security Assertion Markup Language 2.0 (SAML 2.0) is a version
 of the SAML standard for exchanging authentication and authorization data
 between security domains. SAML 2.0 is an XML-based protocol that uses security
 tokens containing assertions to pass information about a principal (usually an
 end user) between a SAML authority, that is, an identity provider, and a SAML
 consumer, that is, a service provider. SAML 2.0 enables web-based
 authentication and authorization scenarios including cross-domain single
 sign-on (SSO), which helps reduce the administrative overhead of distributing
 multiple authentication tokens to the user.
 .
 This package contains the Python 2.x module.

Package: python-pysaml2-doc
Section: doc
Architecture: all
Pre-Depends: dpkg (>= 1.15.6~)
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Description: SAML Version 2 to be used in a WSGI environment - doc
 This package provides a Python implementation of SAML Version 2 to be used in
 a WSGI environment.
 .
 From wikipedia: Security Assertion Markup Language 2.0 (SAML 2.0) is a version
 of the SAML standard for exchanging authentication and authorization data
 between security domains. SAML 2.0 is an XML-based protocol that uses security
 tokens containing assertions to pass information about a principal (usually an
 end user) between a SAML authority, that is, an identity provider, and a SAML
 consumer, that is, a service provider. SAML 2.0 enables web-based
 authentication and authorization scenarios including cross-domain single
 sign-on (SSO), which helps reduce the administrative overhead of distributing
 multiple authentication tokens to the user.
 .
 This package contains the documentation.
